// The "document" refers to the whole webpage.
// The "querySelector" is used to select a specific object (HTML element) from the document(webpage). 

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// Alternatively, we can use the getElement functions to retrieve the element 
	// document.getElementById
	// document.getElementByClassName
	// document.getElementByTagName

// Whenever a user interacts with a web page, this action is considered as an event
// The "addEventListener" is a function that takes two arguements,
	// 1. string identifying the event (keyup)
	// 2. function that the listener will execute once the specified event is triggered(event)
txtFirstName.addEventListener('keyup', (event) => {
	// The "innerHTML" property sets or returns the HTML content
	// innerHTML adds value to spanFullName from txtFirstName value
	// innerHTML manipulates our HTML to add/assign value
	spanFullName.innerHTML = txtFirstName.value;
});

// keyup meaning until you release the letter button, the request is not processed, in this long press of letter c is considered as 1 request when released the key
// keydown meaning the request is prossesed when you enter the letter button in keyboard irrespective of whether the key buttom was released or not, in this long press of letter c is not considered as a single request , it will be the no.of c's in the request ,that no.of requests are generated

// addEventListener listens to txtFirstName
txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	// The "event.target" contains the element where the event happened
	console.log(event.target);
	// The "event.target.value" gets the value of the input object (similar to the textFirstName.value)
	console.log(event.target.value);
});