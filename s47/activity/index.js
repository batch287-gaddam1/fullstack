const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');
const txtLastName = document.querySelector('#txt-last-name');

function updateFullName(){
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
};

txtFirstName.addEventListener('keyup', (event) => {
	updateFullName();
});

txtLastName.addEventListener('keyup', (event) => {
	updateFullName();
});